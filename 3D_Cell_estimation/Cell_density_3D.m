%   3D cell density estimation
%   Based on the quantification of detected 2D cell nuclei, compute the 2D cell density (cell number/tissue area) and average semi-axes of ellipse fitted nuclei contour.
%   Set the initial parameters: r_maj, std_major, r_min, std_min based on the ellipse fitting results.
%   Set the volume, and number of times to repeat the simulation.
%
%   Copyright 2016 Yi Yin
%   $Revision: 1.0 $  $Date: 2016/07/07

%%------------------------------- Initialization -------------------------------
% initial parameters
size_V = 1000;   % volume of the defined cube
N_ellipsoid_initial = 1000;  % initial cell number
r_major = 4.33;    % semi-major axis: Z-axis
r_minor = 2.79;    % semi-minor axis: X-axis,Y-axis
std_major = 2.09;  % standard deviation of semi-major axis
std_minor = 1.30;  % standard deviation of semi-minor axis

N_z_intercept = 50:100:950; %cut plane vertical to Z-axis

N_mc = 10;% number of times to repeat the simulation 

N_ellipsoid_max = 32000; % the maximal number of ellipsoids 
%!!!!!! Note: 32000 is used for test, and 320000 was the value used for our submitted paper
interval_n = 1000; % cell number increment
n_num = N_ellipsoid_max/interval_n;

n_cross_section = length(N_z_intercept);

%%------------------------------- Simulation -------------------------------
N_2D = zeros(n_cross_section*N_mc,n_num);
N_3D = zeros(n_cross_section*N_mc,n_num);

N_2D_mc = zeros(N_mc,n_num);
N_3D_mc = zeros(N_mc,n_num);

for i = 1:n_num
    for i_mc = 1:N_mc
        N_ellipsoid_initial = i*interval_n;
        N_3D(1+(i_mc-1)*n_cross_section:i_mc*n_cross_section,i) = N_ellipsoid_initial;
        N_2D(1+(i_mc-1)*n_cross_section:i_mc*n_cross_section,i) = ellipsoid_creation_rand3_f(size_V,N_ellipsoid_initial,r_major,r_minor,std_major,std_minor,N_z_intercept);
        N_2D_mc(i_mc,i) = mean(N_2D(1+(i_mc-1)*n_cross_section:i_mc*n_cross_section,i));
        N_3D_mc(i_mc,i) = mean(N_3D(1+(i_mc-1)*n_cross_section:i_mc*n_cross_section,i));
    end
end

CD2 = (N_2D)*1e6/(size_V*size_V);
CD3 = (N_3D)*1e9/(size_V^3);

CD2_mc = N_2D_mc.*1e6/(size_V*size_V);
CD3_mc = N_3D_mc.*1e9/(size_V^3);

%save(strcat('CD_3D_Curve_id',num2str(id_cell),'_',Date_run,'.mat'),'size_V','a','b','n_idx','interval_n','n_cross_section','N_mc','CD2', 'CD3','CD20_mc','CD30_mc');

%%------------------------------- Output -------------------------------
CD2_output = reshape(CD2,[],1); % 2D cell density
CD3_output = reshape(CD3,[],1); % 3D cell density

CD_txt = [CD2_output'; CD3_output'];
fileID = fopen('CD_3D_Curve_id.txt','w');
fprintf(fileID,'%d %d\r\n',CD_txt);
fclose(fileID);    

figure,
s = plot(CD2_output',CD3_output','.r');
set(gcf,'color','white');

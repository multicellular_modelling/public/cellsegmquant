%   Ellipsoid creation
%  
%   input parameters
%       size_V              - Volume of cube
%       N_ellipsoid_initial - the initial number of ellipsoids distributed inside the cube
%       a                   - semi-major axis
%       b                   - semi-minor axis
%       N_z_intercept       - number of cross-sections in virtual cutting
%
%   output
%       N_n_cell_cut        - number of the ellipsoid be cut by the given plane
%       ellisoid_all        - information of ellipsoids 
%
%   Copyright 2016 Yi Yin.
%   $Revision: 1.0 $  $Date: 2016/07/07

function [N_n_cell_cut,ellipsoid_all] = ellipsoid_creation_rand3_f(size_V,N_ellipsoid_initial,a,b,c,d,N_z_intercept)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_grid = 50; % number of grid mesh for the ellipsoid visualization

% parameters of the ellipsoid
location_ellipsoid = zeros(N_ellipsoid_initial,3); % location

%[1~3.coordinate of ellipsoid center,4.elevation,5.azimuth]
% ellipsoid_all = zeros(N_ellipsoid_initial,5)
% coordinates of the points on the mesh of the ellipsoid
xd = zeros(N_ellipsoid_initial,(n_grid+1)*(n_grid+1));
yd = zeros(N_ellipsoid_initial,(n_grid+1)*(n_grid+1));
zd = zeros(N_ellipsoid_initial,(n_grid+1)*(n_grid+1));

% parameters of the cross-section (ellipse)
N_n_cell_cut = zeros(length(N_z_intercept),1); % number of the ellipsoid be cut by the given plane
 
% [1.ID_ellipsoid,2.semi-major axis, 3.semi-minor axis,4.orientation,5.center_x,6.center_y,7.center_z (on the cut plane)]
% N_cross_section_ellipse = cell(length(N_z_intercept),1);

%% create result folder
% Path_output = strcat('3D_cell_density','_',num2str(a),'_',num2str(b),'_',num2str(size_V),'_',num2str(N_ellipsoid_initial),'_',Date_run);
% mkdir(Path_output);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ellipsoid %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prevent ellipsoid overlapping
i = 1;
dis_ini= (max(a,b)*2)+1;

while i<=N_ellipsoid_initial 
    location_ellipsoid(i,:) = randi((size_V-floor(max(a,b))),1,3);%rand(1,3).*(size_V-floor(max(a,b)));%
    dis_min = dis_ini; 
    if i>1
        dis_oo = sqrt((location_ellipsoid(1:i-1,1)-location_ellipsoid(i,1)).^2+(location_ellipsoid(1:i-1,2)-location_ellipsoid(i,2)).^2+(location_ellipsoid(1:i-1,3)-location_ellipsoid(i,3)).^2);
        dis_min = min(dis_oo);        
    end

    if dis_min>2*max(a,b)
        i = i+1;
    end
end

%% ellipsoid orientation   
% rotate angle
alph = pi*randi(360,N_ellipsoid_initial,1)/180; % elevation
beta = pi*randi(360,N_ellipsoid_initial,1)/180; % azimuth
gama = pi*randi(360,N_ellipsoid_initial,1)/180;   

r_x = normrnd(b,d,[N_ellipsoid_initial,1]);
r_y = normrnd(a,c,[N_ellipsoid_initial,1]);
r_z = normrnd(sqrt(a*b),sqrt(c*d),[N_ellipsoid_initial,1]);

for i=1:N_ellipsoid_initial
    [x, y, z] = ellipsoid(0,0,0,r_x(i),r_y(i),r_z(i),n_grid);

    x_r = reshape(x,1,[]);
    y_r = reshape(y,1,[]);
    z_r = reshape(z,1,[]);

    Rx = [1 0 0; 0 cos(-alph(i)) -sin(-alph(i)); 0 sin(-alph(i)) cos(-alph(i))];
    Ry = [cos(-beta(i)) 0 sin(-beta(i)); 0 1 0; -sin(-beta(i)) 0 cos(-beta(i))];
    Rz = [cos(-gama(i)) -sin(-gama(i)) 0; sin(-gama(i)) cos(-gama(i)) 0; 0 0 1];

    Mat_orien = Rx*Ry*Rz;

    coor_new = Mat_orien*[x_r; y_r; z_r]; 
    xd(i,:) = coor_new(1,:)+location_ellipsoid(i,1);
    yd(i,:) = coor_new(2,:)+location_ellipsoid(i,2);
    zd(i,:) = coor_new(3,:)+location_ellipsoid(i,3);       
end

%% ellipsoid cutting
for i_section = 1:length(N_z_intercept)
    z_intercept = N_z_intercept(i_section);
   % n_cell_cut = 0;
    dist_zd = sign(zd - z_intercept);    
    idx_0 = find(abs(sum(dist_zd,2)) < (n_grid+1)*(n_grid+1));
    N_n_cell_cut(i_section) = numel(idx_0);
end

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Output %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[1~3.coordinate of ellipsoid center,4.elevation,5.azimuth]
ellipsoid_all = [location_ellipsoid alph beta];



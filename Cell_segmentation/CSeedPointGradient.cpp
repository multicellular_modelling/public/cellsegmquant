// CSeedPointGradient: using the gradient inforamtion to find the initial nuclei seed points
// @author: Yi Yin
// @date: 20 October 2014

#include <string.h>
#include <math.h>
#include <mex.h>
#include <matrix.h>
#include <algorithm>

#define pi 3.1415926535897

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// parameters
	double Rmax, Rmin, l_center, deta_radian, temp_std;
	double *Img_mask, *Gdir, *Gmagni, *GX, *GY;
	
	int size_a, size_b;

	double angle_index, magni_index, Mux, Muy, vector1_x, vector1_y, vector2_x, vector2_y;

	// get the inputvalues
	Img_mask = mxGetPr(prhs[0]);
	Gdir = mxGetPr(prhs[1]);
	Gmagni = mxGetPr(prhs[2]);
	GX = mxGetPr(prhs[3]);
	GY = mxGetPr(prhs[4]);

	Rmax = *mxGetPr(prhs[5]);
	Rmin = *mxGetPr(prhs[6]);
	l_center = *mxGetPr(prhs[7]);
	deta_radian = *mxGetPr(prhs[8]);
	temp_std = *mxGetPr(prhs[9]);
	// size of the input matrix
	size_a = (int)mxGetM(prhs[0]);
	size_b = (int)mxGetN(prhs[0]);	
   
	// array to store the voting image
	plhs[0] = mxCreateDoubleMatrix(size_a,size_b,mxREAL);
	double *Img_V = mxGetPr(plhs[0]);
	int size_c = (int)mxGetM(plhs[0]);
	int size_d = (int)mxGetN(plhs[0]);
	// iterate over all pixel
	for(int i=0; i < size_a; i++)
	{
		for(int j=0; j < size_b; j++)
		{
			Img_V[j*size_a + i] = 0.0;
		}
	}
	for(int i=0; i < size_a; i++)
	{
		for(int j=0; j < size_b; j++)
		{
			// considering the candidate nuclei pixels
			if (Img_mask[j*size_a + i]>0)
			{
				angle_index = Gdir[j*size_a + i];
				magni_index = Gmagni[j*size_a + i];
				Mux = j+1 + l_center*cos(angle_index);
				Muy = i+1 + l_center*sin(angle_index);
				
				vector1_x = -GX[j*size_a + i];
				vector1_y = -GY[j*size_a + i];
				int temp1 = (int)(i-Rmax+1);
				int temp2 = (int)(size_a-1);
				int temp3 = (int)(i+Rmax);
				int temp4 = (int)(j-Rmax+1);
				int temp5 = (int)(size_b-1);
				int temp6 = (int)(j+Rmax+1);
				int r1 = std::max(1,temp1);
				int r2 = std::min(temp2,temp3);
				int r3 = std::max(1,temp4);
				int r4 = std::min(temp5,temp6);

				for (int p = r1; p < r2; p++)
				{
					for (int q = r3; q < r4; q++)
					{
						vector2_x = q - j;
						vector2_y = p - i;
						double dot = vector1_x*vector2_x + vector1_y*vector2_y;      // dot product
						double det = vector1_x*vector2_y - vector1_y*vector2_x;      // determinant
						double angle = abs(atan2(det, dot));  // atan2(y, x) or atan2(sin, cos)
						double dis_pt = sqrt((double(q)-double(j))*(double(q)-double(j)) + (double(p)-double(i))*(double(p)-double(i)));
						if (angle < deta_radian && dis_pt < Rmax && dis_pt > Rmin)
						{
							double tempN = magni_index*(1/(2*pi*temp_std*temp_std))*exp(-((q-Mux)*(q-Mux)+(p-Muy)*(p-Muy))/(2*(temp_std)*(temp_std)));
							double tempV = Img_V[q*size_a + p];
							Img_V[q*size_a + p] = tempV + tempN; // magni_index*(1/(2*pi*temp_std*temp_std))*exp(-((q-Mux)*(q-Mux)+(p-Muy)*(p-Muy))/(2*(temp_std)*(temp_std)));
						}

					}

				}

			}
		}
	}

}


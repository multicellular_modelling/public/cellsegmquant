function [Img_KL1,KL1_dist] = CancerRegionSegm(Img_KL1_deconv)
% Cancer region detection 
Img_KL1_grey = rgb2gray(Img_KL1_deconv); 
Img_KL1_bw = 1-im2bw(Img_KL1_grey, graythresh(Img_KL1_grey));
Img_KL1_morph1 = imdilate(Img_KL1_bw,strel('disk',2));
Img_KL1_morph2 = imclose(Img_KL1_morph1,strel('disk',4));
Img_KL1 = imfill(bwareaopen(Img_KL1_morph2,100),'hole');
% figure,
% imshow(Img_KL1);
KL1_dist = bwdist(Img_KL1); % local minimum region in non-cancer region (non KL1 region)
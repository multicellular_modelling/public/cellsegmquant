function [Num_cell,Data_nuclei,CancerNucleiSegm,nCancerNucleiSegm,Img_nuclei_center] = CellNucleiClass(Img_original,NucleiSegm,KL1_dist)
% cell classification and ellipse fitting

sta = regionprops(NucleiSegm,'all');%'centroid','Area','MajorAxisLength','MinorAxisLength','Orientation');    
n_areas = cat(1,sta.Area);
num_n = length(n_areas);
n_centroids = cat(1,sta.Centroid);
n_majorradius = cat(1,sta.MajorAxisLength);
n_minorradius = cat(1,sta.MinorAxisLength);
n_orientation = cat(1,sta.Orientation);

%% classification of cell types
ID_n_nn = zeros(num_n,1);

for i_nucleus = 1:num_n
    dist_nuclei = KL1_dist(round(n_centroids(i_nucleus,2)),round(n_centroids(i_nucleus,1)));
    if dist_nuclei<3 ||(dist_nuclei/n_areas(i_nucleus)< 0.08) && n_majorradius(i_nucleus)/n_minorradius(i_nucleus)< 2.5 %|| n_areas(i_nucleus)>200
        ID_n_nn(i_nucleus)=1;
    end
end   

%% Save Cancer nuclei segmentation
L_CancerNuclei = bwlabel(NucleiSegm);
CancerNucleiSegm = ismember(L_CancerNuclei,find(ID_n_nn==1));
CancerNucleiSegm(L_CancerNuclei == 0) = 0;
CancerNucleiSegm(CancerNucleiSegm > 0) = 1;

nCancerNucleiSegm = NucleiSegm.*(1-CancerNucleiSegm);

%% Save Data: parameters of tumor and non-tumor nuclei
% Data_nuclei: information of detected cell nuclei
Data_nuclei = [ID_n_nn round(n_centroids(:,2)) round(n_centroids(:,1)) n_areas n_majorradius n_minorradius n_orientation];

num_nuclei_tumor = sum(ID_n_nn);
num_nuclei_nontumor = num_n - num_nuclei_tumor;
Num_cell = [num_n, num_nuclei_tumor, num_nuclei_nontumor];

%% Display the detected centerpoint
Img_nuclei_center = Img_original;
[size_a,size_b] = size(Img_nuclei_center(:,:,1));
Img_tempcc = zeros(size_a,size_b);
Img_tempncc = zeros(size_a,size_b);

for i_nucleus=1:num_n
    x0=round(n_centroids(i_nucleus,1)); % x0,y0 ellipse centre coordinates
    y0=round(n_centroids(i_nucleus,2));   

    if y0>0 && x0>0 && y0<size_a &&x0<size_b
        if ID_n_nn(i_nucleus)==1  
            Img_tempcc(y0,x0) = 1;
        else
            Img_tempncc(y0,x0) = 1;
        end        
    end
end
Img_nuclei_tumor_show = imdilate(Img_tempcc,strel('disk',2));
Img_nuclei_health_show = imdilate(Img_tempncc,strel('disk',2));    
[x_n_tumor,y_n_tumor] = find(Img_nuclei_tumor_show==1);
[x_n_health,y_n_health] = find(Img_nuclei_health_show==1);

for i=1:length(x_n_tumor)
    Img_nuclei_center(x_n_tumor(i),y_n_tumor(i),1)=255;
    Img_nuclei_center(x_n_tumor(i),y_n_tumor(i),2)=0;
    Img_nuclei_center(x_n_tumor(i),y_n_tumor(i),3)=0;
end
for i=1:length(x_n_health)
    Img_nuclei_center(x_n_health(i),y_n_health(i),1)=0;
    Img_nuclei_center(x_n_health(i),y_n_health(i),2)=255;
    Img_nuclei_center(x_n_health(i),y_n_health(i),3)=0;
end         


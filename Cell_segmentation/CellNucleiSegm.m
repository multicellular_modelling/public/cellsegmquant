function NucleiSegm = CellNucleiSegm(KL1_dist,Img_prepros,Img_vote_candidate,Img_vote_final)
% Cell nuclei segmentation
%% rough result of nuclei segmentation --------------------
I1 = Img_prepros;
Ie = imerode(I1, strel('disk',1));
Iobr = imreconstruct(Ie, I1);
% figure,imshow(Iobr), title('Opening-by-reconstruction (Iobr)');

Iobrd = imdilate(Iobr, strel('disk',1));
Iobrcbr = imreconstruct(imcomplement(Iobrd), imcomplement(Iobr));
Iobrcbr = imcomplement(Iobrcbr);
% figure,imshow(Iobrcbr), title('Opening-closing by reconstruction (Iobrcbr)');

% Gaussian bluring
m_mini_seed = fspecial ('gaussian',[3 3],2);
Img_smooth_Iobrcbr = imfilter(Iobrcbr,m_mini_seed);
Img_smooth_nonzero_vir = Img_smooth_Iobrcbr;

bw1 = 1-im2bw(Img_smooth_Iobrcbr, graythresh(Img_smooth_nonzero_vir)*0.5);
bw2 = imclose(bw1, strel(ones(2,2)));
bw3 = imopen(bw2, strel(ones(2,2))); 
bw = bwareaopen(bw3 | Img_vote_candidate, 20) ;%Img_vote_candidate;%
% figure,imshow(bw3), title('Thresholded opening-closing by reconstruction (bw)');

%% local region minimum
Region_min = imregionalmin(Img_smooth_Iobrcbr);  
% Watershed segmentation
% distance map to KL1 staining background
% KL1_dist = bwdist(Img_KL1); % using local minimum region in non-cancer region (non KL1 region)

% nuclei region candidate: gradient vote + local reginal minimum
Img_vote_final = bwareaopen(Img_vote_final,7);
Img_nuclei_bw1 = (Img_vote_final.*(KL1_dist<10) | (Img_vote_final|bwareaopen(Region_min,20)).*(KL1_dist>=10)).*bw;

Img_nuclei_bw2 = imclose(Img_nuclei_bw1, strel(ones(1,1)));
fgm3 = imopen(Img_nuclei_bw2, strel(ones(1,1)));
fgm4 = bwareaopen(fgm3, 3);

D = bwdist(fgm4); 
DL = watershed(D);

NucleiSegm_m = (DL>1).*(bw | fgm4); %| Img_vote_final
NucleiSegm_m = imfill(bwareaopen(NucleiSegm_m,20),'holes');

%% final nuclei segmetation: active contour + remove false detected nuclei
Nuclei_segment_ac = activecontour(Img_prepros, NucleiSegm_m, 10, 'edge');
%figure,imshow(bw);
%% Remove the false nuclei segmentation 
L_nuclei = bwlabel(Nuclei_segment_ac);
NucleiSegm= ismember(L_nuclei,L_nuclei.*fgm4);
NucleiSegm(L_nuclei == 0) = 0;
NucleiSegm(L_nuclei > 0) = 1;

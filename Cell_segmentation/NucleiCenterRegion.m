function [Img_vote_s,Img_vote2,Img_prepros] = NucleiCenterRegion(Img_Hematoxylin_deconv,Segment_KL1)
% using local gradient information to detect the intial seed points (centerpoint of the nucleus)
%% parameters:
%% H staining: Pre-processing & Rough result of nuclei segmentation 
Img_nuclei_CLAHE_grey = adapthisteq(rgb2gray(Img_Hematoxylin_deconv));
% Gaussian kernel
m_mini_seed = fspecial ('gaussian',[5 5],3);
Img_prepros = imfilter(Img_nuclei_CLAHE_grey,m_mini_seed);

%% Gaussian blurred gradient image and the orientation of the gradient 
% Gradient: GY, component in columns; GX, component in rows
[GX, GY] = gradient(double(Img_prepros),3);
% voting area is in the opposit direction of the gradient 
Gdir = atan2(-GY,-GX); 
Gmagni = sqrt(GX.^2+GY.^2);

%% calculation of voting image V(x,y) in a single-path way
% estimated mean nuclei diameter
% cancer cell: 8; non-cancer cell: 5
d = [8;5];
[Img_V_s,Img_vote_s,Img_vote_s2] = SeedPointVotedRegion(d,Gdir,Gmagni,GX,GY,Segment_KL1);

Img_vote = Img_vote_s2;

% morphological operation of voted region 
Img_vote_morph1=Img_vote;
Img_vote_morph2=~bwareaopen(~Img_vote_morph1, 100);
%Img_vote2 = bwareaopen(Img_vote_morph2,10); 
%Img_vote = Img_vote2;
Img_vote2 = Img_vote_s2;





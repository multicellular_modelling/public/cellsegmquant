function [Img_V_l,Img_vote_final,Img_vote_final2] = SeedPointVotedRegion(d,Gdir,Gmagni,GX,GY,Segment_KL1)
%% parameters:
[size_a,size_b] = size(GX);

%% calculation of voting image V(x,y) in a single-path way
%l_center = (Rmax + Rmin)/2;
Img_mask = ones(size_a,size_b);

% d: estimated mean nuclei diameter
d_l = d(1);% estimated mean nuclei diameter
Rmin = round(0.5*d_l); % parameter used to define the voting area
Rmax = round(1.5*d_l); % parameter used to define the voting area
delta = 30; % angle of the cone shape voting area
delta_radian = delta*pi/180;
l_center = (Rmax + Rmin)/2;
temp_std = delta*((Rmax + Rmin)/2)/2; % std of the shifted Gaussian filter
Img_V_l = CSeedPointGradient(Img_mask,Gdir,Gmagni,GX,GY,Rmax,Rmin,l_center,delta_radian,temp_std);

m_V_l = fspecial ('gaussian',[3 3],4);
Img_V_l = imfilter(Img_V_l,m_V_l);

se=strel('disk',2);
I1 = imcomplement(Img_V_l);
Ie = imerode(I1, se);
Iobr = imreconstruct(Ie, I1);

Iobrd = imdilate(Iobr, se);
Iobrcbr = imreconstruct(imcomplement(Iobrd), imcomplement(Iobr));
Iobrcbr = imcomplement(Iobrcbr);
fgm = imregionalmin(Iobrcbr);

d_s = d(2);% estimated mean nuclei diameter
Rmin = round(0.5*d_s); % parameter used to define the voting area
Rmax = round(1.5*d_s); % parameter used to define the voting area
delta = 30; % angle of the cone shape voting area
l_center = (Rmax + Rmin)/2;
delta_radian = delta*pi/180;
temp_std = delta*((Rmax + Rmin)/2)/2; % std of the shifted Gaussian filter
Img_V_s = CSeedPointGradient(Img_mask,Gdir,Gmagni,GX,GY,Rmax,Rmin,l_center,delta_radian,temp_std);

% fgm2 = imregionalmin(imcomplement(Img_V_s));
% figure,imshow(fgm2);
m_V_s = fspecial ('gaussian',[3 3],2);
Img_V_s = imfilter(Img_V_s,m_V_s);

se=strel('disk',1);
I1 = imcomplement(Img_V_s);
Ie = imerode(I1, se);
Iobr = imreconstruct(Ie, I1);

Iobrd = imdilate(Iobr, se);
Iobrcbr = imreconstruct(imcomplement(Iobrd), imcomplement(Iobr));
Iobrcbr = imcomplement(Iobrcbr);
fgm2 = imregionalmin(Iobrcbr);

% local gradient region
Max_vote = max(max(Img_V_l));
Img_vote = zeros(size_a,size_b);
for m = 1:7
    T_seed = (m+2)*0.11*Max_vote; % the scale is 0.14 before 20150602
    temp_V = Img_V_l;
    temp_V(temp_V>T_seed) = 1;
    temp_V(temp_V<=T_seed) = 0;
    Img_vote = Img_vote + temp_V;
end

%
Max_vote_s = max(max(Img_V_s));
Img_vote_s = zeros(size_a,size_b);
for m = 1:7
    T_seed = (m+2)*0.11*Max_vote_s; % the scale is 0.14 before 20150602
    temp_V = Img_V_s;
    temp_V(temp_V>T_seed) = 1;
    temp_V(temp_V<=T_seed) = 0;
    Img_vote_s = Img_vote_s + temp_V;
end

%
Img_vote_final = Img_vote.*Segment_KL1 + Img_vote_s.*(1-Segment_KL1);
Img_vote_final2 = fgm.*Segment_KL1 + fgm2.*(1-Segment_KL1)|Img_vote_s;
% voting image with all the candidate see points
%Img_vote = Img_vote + seed_points;





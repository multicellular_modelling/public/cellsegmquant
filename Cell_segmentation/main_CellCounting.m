%   function [Num_cell,Data_nuclei,NucleiSegm,CancerNucleiSegm,nCancerNucleiSegm] = CellCounting(Image_original)
%   Matlab toolbox for cell quantification in histological data. 
%   The algorithm combines:
%   1) Color deconvolution to separate the color image into specific channels: hematoxylin for nucleus segmentation and KL1 for the cancer maker segmentation.
%   2) Cell nuclei segmentation based on local intensity minimum and gradient information 
%   3) Ellipsoid model-based 3D cell density estimation
%
%   The algorithm description is available at the manuscript submitted to IEEE T-MI
%
%   [Num_cell,Data_nuclei,NucleiSegm,CancerNucleiSegm,nCancerNucleiSegm] = CellCounting(Img_original)
%   Input Parameters:
%       Img_original        -  the input image
%
%   Output_Parameters:
%       Num_cell            - detected cell numbers
%       Data_nuclei         - Info of detected cell nucleus
%       NucleiSegm          - Total cell nuclei segmentation
%       CancerNucleiSegm    - cancer nuclei segmentation
%       nCancerNucleiSem    - non-cancer nuclei segmentation

%   Example:
%       [Num_cell,Data_nuclei,NucleiSegm,CancerNucleiSegm,nCancerNucleiSegm] = CellCounting('test.tif')
%
%
%   Copyright 2016 Yi Yin.
%   $Revision: 1.0 $  $Date: 2016/07/07

%% ------------------------------ Parameter settings------------------------
% the default image resolution is 0.466 micron/pixel
Img_original = imread('test1.tif');

% Call Mex functions
mex CSeedPointGradient.cpp;

%% ------------------------------ Preprocessing -------------------------------

% Color de-convolution: hematoxylin channel and KL1 channel
[Img_Hematoxylin_deconv,Img_KL1_deconv] = CDeconv(Img_original);

%% ---------------------- KL1 staining: cancer sensitive region ----------------------
[Img_KL1,KL1_dist] = CancerRegionSegm(Img_KL1_deconv);

%% --------------------------- Nuclei center regions ----------------------------------
% Gradient based voting region: using local gradient information 
[Img_vote_candidate,Img_vote_final,Img_prepros] = NucleiCenterRegion(Img_Hematoxylin_deconv,Img_KL1);

%% ----------------------------- nuclei segmentation ---------------------------------
NucleiSegm = CellNucleiSegm(KL1_dist,Img_prepros,Img_vote_candidate,Img_vote_final);

%% ---------------------------- nuclei classification ---------------------------------
[Num_cell,Data_nuclei,CancerNucleiSegm,nCancerNucleiSegm,Img_nuclei_center] = CellNucleiClass(Img_original,NucleiSegm,KL1_dist);

%% Output
% Binary segmentation result: NucleiSegm (total cell); CancerNucleiSegm (cancer cell); nCancerNucleiSegm(non-cancer cell)
% Result image: Img_nuclei_center, detected nuclei center points (red: cancer cells and green: non-cancer cells)
imwrite(Img_nuclei_center,'Img_center.tif', 'compression','none');

% cell number: total number, number of cancer cell, number of non-cancer cell

% Information of detected cell nuclei: Data_nuclei
% Data_nuclei(:,1) = ID_n_nn'; % 1 means cancer cell, 0 means non-cancer cell
% Data_nuclei(:,2) = round(n_centroids(:,2)); % center coordinates
% Data_nuclei(:,3) = round(n_centroids(:,1)); % center coordinates
% Data_nuclei(:,4) = n_areas; % area of cell nucleus
% Data_nuclei(:,5) = n_majorradius; % semi-major axis of ellipse-fitted nucleus contour
% Data_nuclei(:,6) = n_minorradius; % semi-minor axis of ellipse-fitted nucleus contour
% Data_nuclei(:,7) = n_orientation; % orientation of fitted ellipse



